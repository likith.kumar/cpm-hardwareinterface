# import board
# import busio
# import adafruit_pca9685

# i2c = busio.I2C(board.SCL, board.SDA)
# pca = adafruit_pca9685.PCA9685(i2c)
# #pca = adafruit_pca9685.PCA9685(address=0x40, i2c_bus=1)
# pca.frequency = 60
# v=0
# x=2048
# #pca.get_pin(0).direction = PCA9685.IN
# try:
#     while True:
#         print("value: ",pca.get_pin(0))
# except KeyboardInterrupt:
#     pass

# # import smbus2
# # import time 
# # busn=7
# # deviceaddr="0x00"
# # bus = smbus2.SMBus(busn)
# # try:
# #     while True:
# #         data = bus.read_byte(deviceaddr)
# #         print(data)
# # except KeyboardInterrupt:
# #     bus.close()
# #     print("closed")


import time
import serial

print("Speed Sensor Values.")


serial_port = serial.Serial(
    port="/dev/ttyTHS0",
    baudrate=115200,
    bytesize=serial.EIGHTBITS,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
)
time.sleep(1)
# count = 0
number_of_holes = 12
try:
    count = 0
    start_time=time.time()
    while True:
        if serial_port.inWaiting() > 0:
            
            speed_data_low = (serial_port.read())
            if(speed_data_low):
                count+=1
            # if(time.time()-start_time>1e-05):
            #     one_rotation = count/number_of_holes
            #     count=0
            #     print(one_rotation*6e05)
            #     start_time=time.time()
            if((count>0) and (time.time()-start_time > 1)):
                one_rotation = count/number_of_holes    
                rpm= (60*1000)/(time.time()-start_time)*one_rotation
                count=0 
                print(rpm)
                start_time=time.time()

except KeyboardInterrupt:
    print("Exiting Program")

except Exception as exception_error:
    print("Error occurred. Exiting Program")
    print("Error: " + str(exception_error))

finally:
    serial_port.close()
    pass
